CXX := g++

CXXFLAGS=-g -std=c++11 -Wall -pedantic

all: libSimpleStack.so testSimpleStack testOtherStuff

simpleStack.o: simpleStack.cpp
	$(CXX) -fPIC -Wall -c -o simpleStack.o simpleStack.cpp

libSimpleStack.so: simpleStack.o
	$(CXX) -fPIC -shared -Wl $^ -o $@

testSimpleStack: libSimpleStack.so
	$(CXX) -o testSimpleStack -L./ testSimpleStack.cpp -lSimpleStack

testOtherstuff: 
	$(CXX) -o testOtherStuff testOtherStuff.cpp 

clean:
	$(RM) *.o *.so*
	$(RM) testSimpleStack
	$(RM) testOtherStuff
