#include <cstdint>
#include <cstdlib>

#include "simpleStack.h"

simpleStack::simpleStack()
{
	numItems = 0;
	top = NULL;
}
simpleStack::~simpleStack()
{
	simpleStackItem * thisItem = top;
	simpleStackItem * nextItem;
	while (thisItem != NULL)
	{
		nextItem = thisItem->next;
		delete thisItem;
        thisItem = nextItem;
	}
}

// push(thisValue) - adds thisValue to the top of the stack. 
// returns STACK_ERR_FULL if it fails to allocate the new item
// returns STACK_ADD_SUCCESS if successful
//
int simpleStack::push(int thisValue)
{
	simpleStackItem * newItem = new simpleStackItem;
	if (newItem == NULL)
		return STACK_ERR_FULL;

	newItem->value = thisValue;
	newItem->next  = this->top;
	this->top = newItem;
	this->numItems++;

	return STACK_ADD_SUCCESS;
}

// pop() - deletes the topmost item from the stack, returns its value
//       - throws STACK_ERR_EMPTY on empty stacks
//
int simpleStack::pop(void)
{
	if (this->top == NULL)
	{
		// this makes me really unhappy!
		throw STACK_ERR_EMPTY;
	}

	simpleStackItem * itemToPop = this->top;
	this->top = this->top->next;

	int returnVal = itemToPop->value;
	delete itemToPop;
	this->numItems--;

	return returnVal;	
}

// peek() - returns the topmost value in the stack
//        - throws STACK_ERR_EMPTY on empty stacks
//
int simpleStack::peek(void)
{
	if (this->top == NULL)
	{
		throw STACK_ERR_EMPTY;
	}

	return this->top->value;
}

// peekPtr() - returns a pointer to the integer at the top of the stack
//           - returns NULL if the stack is empty
//           - is a bonus function lol
// 
int * simpleStack::peekPtr(void)
{
	if (this->top == NULL)
		return NULL;
	else
		return &this->top->value;
}

// size() - returns a size_t number of items in the stack
// 
size_t simpleStack::size(void)
{
	return this->numItems;
}
