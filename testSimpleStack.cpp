#include "simpleStack.h"
#include <stdio.h>
#include <assert.h>
#include <iostream>

int main(void)
{
	simpleStack * thisStack = new simpleStack;


    std::cout << "The basic simpleStack test verifies an empty simpleStack." << std::endl;

	// First, the basics. Test the constructor
	//
	assert(thisStack != NULL);
	assert(thisStack->size() == 0);
	assert(thisStack->peekPtr() == NULL);

	int retVal = 0;

	// Try to pop the empty stack
	//
	try
	{
		retVal = thisStack->pop();
	}
    catch (int errNum)
    {
        std::cout << "Popping empty stack resulted in this exception: " << errNum << std::endl;
        assert(errNum == STACK_ERR_EMPTY);
    }

	std::cout << "Basic test passed!" << std::endl;

    // Add a couple of items, verify the count, and pop them!
    //

    int firstVal = 42;
    int secondVal = 52; 

    assert(thisStack->push(firstVal) == STACK_ADD_SUCCESS);
    assert(thisStack->size() == 1);

    std::cout << "first item successfully added to stack" << std::endl;

    assert(thisStack->push(secondVal) == STACK_ADD_SUCCESS);
    assert(thisStack->size() == 2);

    std::cout << "second item successfully added to stack" << std::endl;

    assert(thisStack->pop() == secondVal);
    assert(thisStack->size() == 1);

    std::cout << "second item successfully popped from stack" << std::endl;

    assert(thisStack->pop() == firstVal);
    assert(thisStack->size() == 0);

    std::cout << "first item successfully popped from stack" << std::endl;

    // Try to pop the empty stack .. again!
    //
    try
    {
        retVal = thisStack->pop();
    }
    catch (int errNum)
    {
        std::cout << "Popping empty stack resulted in this exception: " << errNum << std::endl;
        assert(errNum == STACK_ERR_EMPTY);
    }
    std::cout << "add 2 and pop 3 test passed!" << std::endl;


    // Now let's try peeking
    // 
    int thirdVal = 256;
    assert(thisStack->push(thirdVal) == STACK_ADD_SUCCESS);
    assert(thisStack->size() == 1);
    std::cout << "third item successfully added to stack" << std::endl;

    assert(thisStack->peek() == thirdVal);
    std::cout << "peek successful" << std::endl;

    assert(*thisStack->peekPtr() == thirdVal);
    std::cout << "peekPtr successful" << std::endl;

    delete thisStack;


	return 0;
}