#include <cstdint>
#include <cstdlib>

#define STACK_ERR_EMPTY 25
#define STACK_ERR_FULL  -1
#define STACK_ADD_SUCCESS 0

struct simpleStackItem
{
    int value;
    simpleStackItem * next;
};

// simpleStack is a container with the usual push, pop, peek, and size functions. 
// to use it: simpleStack * thisStack = new simpleStack;
class simpleStack
{
public:
    simpleStack();
    ~simpleStack();

    // push(thisValue) - adds thisValue to the top of the stack. 
    // returns STACK_ERR_FULL if it fails to allocate the new item
    // returns STACK_ADD_SUCCESS if successful
    //
    int push(int thisValue);

    // pop() - deletes the topmost item from the stack, returns its value
    //       - throws STACK_ERR_EMPTY on empty stacks
    //
    int pop(void);

    // peek() - returns the topmost value in the stack
    //        - throws STACK_ERR_EMPTY on empty stacks
    //
    int peek(void);

    // peekPtr() - returns a pointer to the integer at the top of the stack
    //           - returns NULL if the stack is empty
    //           - is a bonus function lol
    // 
    int * peekPtr(void);

    // size() - returns a size_t number of items in the stack
    // 
    size_t size(void);

// here be dragons
//
private:
    simpleStackItem * top;
    size_t numItems;
};
