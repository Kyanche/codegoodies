#include <vector>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <assert.h>
using namespace std;

// findDuplicate() - returns the duplicate int in an unsorted vector of ints
// * `input` contains N+1 numbers
// * `input` elements are integers in the domain [1, N] 
// * `input` contains all integers in the domain [1, N] at least once
//
int findDuplicate(std::vector<int> const& input) 
{
    vector<int> thisCount(input.size());
    fill(thisCount.begin(), thisCount.end(), 0);

    // make thisCount[x] = the number of times x shows up in input[]
    //
    for(int i : input)    
    {
        thisCount[i]++;
        if (thisCount[i] == 2)
            return i;
    }

    return 0;
}

#define NOT_ALLOC_BY_US -25

// because nobody said this had to be efficient
//

#define ALIGN_SYNC_WORD 0x11ba07f0
struct alignedAllocMetaData
{
    uint32_t syncWord;
    uint8_t * origin;
};


// Allocates a block of memory with at least `size` bytes available.
// Returned pointer is guaranteed to be aligned on an `alignment` byte boundary.
//
void* aligned_alloc(size_t size, size_t alignment) 
{
    // you'd think that after writing flight software I'd see this more often? 
    // but I haven't yet. I'm going to keep an eye out for it!
    //
    // apparently c++17 has a proposal for JUST this thing. 
    // http://open-std.org/JTC1/SC22/WG21/docs/papers/2012/n3396.htm
    // oh yeah, and we can't use posix_memalign. But we can use malloc and free
    // and new/delete. Oh goodie. That means I get to play memory roulette! 
    //

    // My strategy will be to grab size+2*alignment, that way I get a continuous block
    // and can delete up to x=2*alignment bytes! (and have room for metadata)
    // 
    void * origin = malloc(size+2*alignment+sizeof(alignedAllocMetaData));
    cout << origin << endl;

    void * retVal = ((uint8_t*)origin + sizeof(alignedAllocMetaData));

    // advance the pointer past the metadata
    //
    size_t alignment_offset = 0;
    while (((uintptr_t)(const void *)retVal % alignment) != 0)
    {
        alignment_offset++;
        retVal = ((uint8_t*)retVal)+1;
    }
    cout << retVal << endl;
    cout << "alignment_offset = " << alignment_offset << endl;


    // Great, now that we know that retVal = origin + alignment_offset, 
    // let's put the metadata at retval - sizeof(alignedAllocMetadata)
    //

    alignedAllocMetaData * thisMetaData = (alignedAllocMetaData *)( ((uint8_t*)retVal) - sizeof(alignedAllocMetaData));
    thisMetaData->syncWord = ALIGN_SYNC_WORD;
    thisMetaData->origin = (uint8_t *)origin;


    // I'm just filling in extra 0x5A because I feel like it.
    // 
    // [0-alignment_offset = 5A][alignedAllocMetaData][actual data][2*alignment-alignment_offset]
    memset(origin, 0x5A, alignment_offset);
    memset(((uint8_t *)retVal)+size, 0x5A, (2*alignment)-alignment_offset);
    memset(retVal, 0x00, size);

    printf("Data test! \n");
    int i = 0;
    for (i = 0; i < size+sizeof(alignedAllocMetaData)+2*alignment; i++)
    {
        printf("%02X ", ((uint8_t*)origin)[i]);
    }
    printf("\n");

    assert(((uintptr_t)(const void *)retVal % alignment) == 0);
    printf("Test passed\n");

    return retVal;
}

// Accepts an aligned pointer and releases the correct block of memory.
// Input of nullptr is acceptable and must do nothing.
void aligned_free(void* ptr) 
{
    alignedAllocMetaData * thisMetaData = (alignedAllocMetaData *)( ((uint8_t*)ptr) - sizeof(alignedAllocMetaData));

    if (thisMetaData->syncWord != ALIGN_SYNC_WORD)
    {
        printf("It doesn't look like this pointer was allocated by aligned_alloc! \n");
        throw NOT_ALLOC_BY_US;
    }
    uint8_t * thisOrigin = thisMetaData->origin;
    free(thisOrigin);
}

// 4. Implement a blitting function which supports color-keyed transparency.

unsigned int const COLOR_KEY{0xFF000000};

struct PixelBuffer {
  uint32_t *pixels;
  int width;
  int height;
};

// Copies the entire image from `src` into a destination buffer `dest`.
// The pixel buffers have a top-left origin and are row-major.
// `offsetX` and `offsetY` denote the origin within `dest` where `src` should be
// copied.
// Any pixel that exactly matches `COLOR_KEY` should be skipped.
// You may assume input buffers are pre-allocated and sufficiently large to
// complete the requested operation.
void blit(PixelBuffer const* src, PixelBuffer* dest, size_t offsetX, size_t offsetY) 
{
    int x = 0, y = 0;

    // offsetX and offsetY are size_t, so they are never negative/
    // from the wording of the problem, I assume they are always 
    // (x,y) inside dest. 
    // 
    int yMax = dest->height - offsetY;
    if (src->height < yMax)
        yMax = src->height;

    int xMax = dest->width - offsetX;
    if (src->width < xMax)
        xMax = src->width;

    // If src resides entirely in dest, such that
    // there are elements of dest to the right of src
    // like so:
    // [xxx[yyyy]xx]
    // 
    // then addX is the number of elements in dest, to the
    // right of src (per line)
    // 
    int addDestX = dest->width - offsetX - src->width + 1;
    if (addDestX < 0)
        addDestX = 0;

    // However, if src extends past the edge of dest, we need to
    // addSrcX! 
    // 
    int addSrcX = src->width + offsetX - dest->width + 1;
    if (addSrcX < 0)
        addSrcX = 0;


    uint32_t * destPixel = dest->pixels + (dest->width * offsetY);

    // We can always assume (offsetX, offsetY) == (srcX == 0, srcY == 0)
    //
    uint32_t * srcPixel = src->pixels; 

    // And now, let the blitting begin!
    //
    for (y = 0; y < yMax; y++)
    {
        // When we start a new line, we're at dest[offset Y + y][0]
        // advance to the beginning of src
        // 
        destPixel += offsetX;
        for (x = 0; x < src->width; x++)
        {
            // Now, for that copy condition!
            // 
            if (*srcPixel != COLOR_KEY)
            {
                *destPixel = *srcPixel;
            }
            destPixel++;
        }

        // When we're done with a line, we're at dest[offset Y + y][0 + src->width]
        // advance to dest[offset Y + y][0 + src->width + addDestX]
        // (this is effectively dest[offSet Y + y + 1][0])
        //
        destPixel += addDestX;
        srcPixel += addSrcX;
    }
}


int main()
{
    cout << "Trying to find the duplicate number in the vector {2,5,3,4,2,1}" << endl;
    vector<int> newVector = {2, 5, 3, 4, 2, 1};
    assert(findDuplicate(newVector) == 2);
    cout << "Test passed!" << endl << endl;

    cout << "Trying to find the duplicate number in the vector {2,5,3,4,2,1}" << endl;
    vector<int> uselessVector = {6, 5, 3, 4, 2, 1};
    assert(findDuplicate(uselessVector) == 0);
    cout << "Test passed!" << endl << endl;


    cout << "Testing aligned alloc on a 256-byte chunk of memory aligned on 128-byte boundaries" << endl;
    void * randomCrap = aligned_alloc(256, 128);
    assert(randomCrap != NULL);
    cout << "Test passed! (We got a pointer that wasn't NULL)" << endl << endl;

    cout << "Testing aligned_free()" << endl;
    aligned_free(randomCrap);
    cout << "Hey, we didn't crash! Cool. That's good enough for me." << endl << endl;


    return 0;
}