# Code Goodies
Here's a small collection of C++ code snippets. 

## simpleStack
A C++ library that uses a linked-list model to build a stack of integers. You can push to it, pop from it, and peek at it. I've added a little peekPtr feature that allows you to view and change the top pointer without having to push/pop. 

## testSimpleStack 
A test app for the simpleStack object - it shows you basic usage and makes sure everything works as advertised

## testOtherStuff
Will be used to try out the other test examples. More to come soon!